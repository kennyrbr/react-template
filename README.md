# react-template

A React TypeScript Webpack starter for creating React projects. 

Started as a walkthrough for my own learning purposes of the excellent work at https://github.com/gopinav/React-TypeScript-Webpack-Starter and went from there. 