import './styles.css'
import IMAGE from './react-logo.png'
import LOGO from './react-logo.svg'
import { ClickCounter } from './ClickCounter'

export const App = () => {
  return (
    <>
      <h1>
        {`React TypeScript Webpack Starter Template - ${process.env.NODE_ENV}  ${process.env.name}`}
      </h1>
      <img src={IMAGE} alt="React PNG Logo!!" />
      <img src={LOGO} alt="React SVN Logo" />

      <ClickCounter />
    </>
  )
}
